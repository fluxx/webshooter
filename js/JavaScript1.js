﻿playerPosX = 100;
playerPosY = 100;

var KEYCODE = {
    87:'w',
    65: 'a',
    83: 's',
    68: 'd'
}

var wasdInput = {
    87: false,  //W
    65: false,  //A
    83: false,  //S
    68: false   //D
};

var mousePos = new Vector2D(0, 0);

var game = {};
var c;
var player;

window.onload = function () {
    
    document.captureEvents(Event.MOUSEMOVE);
    document.captureEvents()

    c = initCanvas();

    player = new Player(playerPosX, playerPosY, 4);

    requestAnimationFrame(gameLoop);
};


function gameLogic() {

    vectoreToMouse = mousePos.substract(player.pos);
    
    player.move();
    
};



function draw() {

    //Fill Background
    c.fillStyle = "#FEFEFE";
    c.fillRect(0, 0, c.canvas.width, c.canvas.height);

    document.getElementById("mouseX").innerHTML = mousePos.x;
    document.getElementById("mouseY").innerHTML = mousePos.y;

    document.getElementById("moveX").innerHTML = player.moveDirection.x;
    document.getElementById("moveY").innerHTML = player.moveDirection.y;

    document.getElementById("moveSpeed").innerHTML = player.moveSpeed;

    player.draw();

    //draw cursor
    c.strokeStyle = "#DD0505";
    c.beginPath();
    c.arc(mousePos.x, mousePos.y, 5, 0, 2 * Math.PI);
    c.lineWidth = 2;
    c.stroke();

}


function gameLoop() {

    
    gameLogic();


    draw();


    requestAnimationFrame(gameLoop);

};


function initCanvas() {

    var canvas = document.getElementById("screen");
    var c = canvas.getContext("2d");

    c.canvas.width = window.innerWidth;
    c.canvas.height = window.innerHeight;

    //Fill Background
    c.fillStyle = "#FEFEFE";
    c.fillRect(0, 0, c.canvas.width, c.canvas.height);

    return c;

};


document.onmousemove = function (e) {

    mousePos.x = e.clientX;
    mousePos.y = e.clientY;

}

document.addEventListener('keydown', function (event) {
    wasdInput[event.keyCode] = true;
});

document.addEventListener('keyup', function (event) {
    wasdInput[event.keyCode] = false;
});


//CONSTRUCTOR FUNCTIONS
function Player(x, y, moveSpeedMax) {
    this.pos = new Vector2D(x, y);
    this.moveSpeed = 0;
    this.moveSpeedMax = moveSpeedMax;
    this.accelaration = 0.3;
    this.moveDirection = new Vector2D(0, 0);
    this.aim = new Vector2D(0, 0);

    this.draw = function () {

        c.fillStyle = "#1010FF";
        c.beginPath();
        c.arc(this.pos.x, this.pos.y, 14, 0, 2 * Math.PI);
        c.fill();

    };

    
    
    this.move = function () {
        this.calcMoveDirection();
        if (this.moveSpeed < this.moveSpeedMax && (this.moveDirection.x != 0 || this.moveDirection.y != 0)) {
            this.moveSpeed += this.accelaration;
        }
        else if (this.moveSpeed > this.moveSpeedMax) {
            this.moveSpeed = this.moveSpeedMax;
        }
        else if (this.moveSpeed > 0 && this.moveDirection.x == 0 && this.moveDirection.y == 0) {
            this.moveSpeed = 0;
        }

        this.pos = this.pos.add(this.moveDirection.scale(this.moveSpeed));
        console.log(this.pos.x, this.pos.y);
    };


    this.calcMoveDirection = function () {
            var keyCombination = "";
            for (key in wasdInput) {
                if (wasdInput[key]) {
                    keyCombination += KEYCODE[key];
                }
            }

            switch (keyCombination) {
                case "w": this.moveDirection.x = 0; this.moveDirection.y = -1; break;
                case "a": this.moveDirection.x = -1; this.moveDirection.y = 0; break;
                case "s": this.moveDirection.x = 0; this.moveDirection.y = 1; break;
                case "d": this.moveDirection.x = 1; this.moveDirection.y = 0; break;

                case "wd": this.moveDirection.x = 0.7071; this.moveDirection.y = -0.7071; break;
                case "dw": this.moveDirection.x = 0.7071; this.moveDirection.y = -0.7071; break;

                case "wa": this.moveDirection.x = -0.7071; this.moveDirection.y = -0.7071; break;
                case "aw": this.moveDirection.x = -0.7071; this.moveDirection.y = -0.7071; break;

                case "sd": this.moveDirection.x = 0.7071; this.moveDirection.y = 0.7071; break;
                case "ds": this.moveDirection.x = 0.7071; this.moveDirection.y = 0.7071; break;

                case "sa": this.moveDirection.x = -0.7071; this.moveDirection.y = 0.7071; break;
                case "as": this.moveDirection.x = -0.7071; this.moveDirection.y = 0.7071; break;

                default: this.moveDirection.x = 0; this.moveDirection.y = 0;
            }
        };

};


function Vector2D(x, y) {

    this.x = x;
    this.y = y;

    this.getLength = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    };

    this.normalize = function () {
        var l = this.getLength();
        return new Vector2D(this.x / l, this.y / l);
    };

    this.scale = function (s) {
        return new Vector2D(this.x * s, this.y * s);
    };

    this.add = function (v) {
        return new Vector2D(this.x + v.x, this.y + v.y);
    };

    this.substract = function (v) {
        return new Vector2D(this.x - v.x, this.y - v.y);
    }

};

/*
*   Creates a Projectile
*   Projectile(Vector2D, Vector2D, int)
*/
function Projectile(pos, moveDirection, speed) {

    this.pos = pos;
    this.moveDirection = moveDirection;
    this.speed = speed;

    this.move() = function () {
        this.pos = this.pos.add(this.moveDirection.scale(speed));
    };

}